package com.truam.funmath.android.ui.normal

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Vibrator
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.truam.funmath.android.R
import com.truam.funmath.android.databinding.ActivityHardModeBinding
import com.truam.funmath.android.util.AppDataUtil
import com.truam.funmath.android.widget.CustomDialog
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.*

/**
 * Copyright © 2017 Truam Studio.
 * All Rights Reserved
 *
 * Created on 19-Dec-17.
 * by Nguyen Phuong Tuan
 */

class FactoryModeActivity : AppCompatActivity() {

    companion object {
        val EXTRA_MODE = "EXTRA_MODE"
        val EXTRA_MODE_NORMAL = "EXTRA_MODE_NORMAL"
        val EXTRA_MODE_HARD = "EXTRA_MODE_HARD"
    }

    private var binding: ActivityHardModeBinding? = null
    private var viewModel: FactoryModeViewModel? = null
    private lateinit var mInterstitialAd: InterstitialAd
    private var isDialogShowing = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hard_mode)
        viewModel = ViewModelProviders.of(this).get(FactoryModeViewModel::class.java)

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/TextFont.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )

        val adRequest = AdRequest.Builder().build()
        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = "ca-app-pub-4412751318966463/8594676757"
        mInterstitialAd.loadAd(adRequest)
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                mInterstitialAd.loadAd(adRequest)
            }
        }

        if (intent.extras.containsKey(EXTRA_MODE)) {
            if (intent.extras.getString(EXTRA_MODE) == EXTRA_MODE_HARD) {
                binding?.textViewModeName?.text = getString(R.string.splash_screen_hard_title)
                viewModel?.setModeInfo(EXTRA_MODE_HARD)
            } else {
                binding?.textViewModeName?.text = getString(R.string.splash_screen_normal_title)
                viewModel?.setModeInfo(EXTRA_MODE_NORMAL)
            }
        }

        binding?.buttonBack?.setOnClickListener({
            onBackPressed()
        })

        binding?.progressBar?.indeterminateDrawable?.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY)

        viewModel?.number1?.observe(this, Observer {
            binding?.number1?.text = String.format("%d", it)
        })

        viewModel?.number2?.observe(this, Observer {
            binding?.number2?.text = String.format("%d", it)
        })

        viewModel?.number3?.observe(this, Observer {
            binding?.number3?.text = String.format("%d", it)
        })

        viewModel?.currentScore?.observe(this, Observer {
            binding?.score?.text = String.format("%d", it)
        })

        viewModel?.backGroundColor?.observe(this, Observer {
            it?.let {
                binding?.root?.setBackgroundColor(ContextCompat.getColor(this, it))
            }
        })

        binding?.buttonTrue?.setOnClickListener {
            if (!isDialogShowing) {
                viewModel?.onChooseAnswer(true)
            }
        }

        binding?.buttonFalse?.setOnClickListener {
            if (!isDialogShowing) {
                viewModel?.onChooseAnswer(false)
            }
        }

        viewModel?.showDialog?.observe(this, Observer {
            it?.let {
                showDialog(it)
            }
        })

        viewModel?.progress?.observe(this, Observer {
            it?.let {
                binding?.progressBar?.progress = it
            }
        })

        viewModel?.refreshQuestion()
    }

    private fun showDialog(message: String) {
        if (isDialogShowing) {
            return
        }
        isDialogShowing = true
        (getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).vibrate(200)
        val curScore = viewModel?.currentScore?.value
        var highScore = AppDataUtil.getNormalHighScore()
        if (intent.extras.containsKey(EXTRA_MODE)) {
            if (intent.extras.getString(EXTRA_MODE) == EXTRA_MODE_HARD) {
                highScore = AppDataUtil.getHardHighScore()
            }
        }
        if (curScore != null && highScore != null) {
            val dialog = CustomDialog.newInstance(message, String.format("%d", curScore), String.format("%d", highScore))
            dialog.setOnClickListener(View.OnClickListener {
                dialog.dismiss()
                isDialogShowing = false
                viewModel?.newGame()
                showAdmob()
            })
            dialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
            dialog.show(supportFragmentManager, "Dialog")
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun showAdmob() {
        val num = Random().nextInt(5)
        if (num == 2) {
            if (mInterstitialAd.isLoaded) {
                mInterstitialAd.show()
            }
        }
    }
}
