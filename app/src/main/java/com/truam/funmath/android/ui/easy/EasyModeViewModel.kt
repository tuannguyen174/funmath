package com.truam.funmath.android.ui.easy

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.truam.funmath.android.R
import com.truam.funmath.android.util.AppDataUtil
import com.truam.funmath.android.util.FunMathUtil
import java.util.*

/**
 * Copyright © 2017 Truam Studio.
 * All Rights Reserved
 *
 * Created on 19-Dec-17.
 * by Nguyen Phuong Tuan
 */

class EasyModeViewModel : ViewModel() {
    var number1 = MutableLiveData<Int>()
    var number2 = MutableLiveData<Int>()
    var number3 = MutableLiveData<Int>()
    var currentScore = MutableLiveData<Int>()
    var showDialog = MutableLiveData<String>()
    var backGroundColor = MutableLiveData<Int>()
    private var positiveAnswer = true

    fun refreshQuestion() {
        if (currentScore.value == null) {
            currentScore.value = 0
        }
        if (backGroundColor.value == null) {
            backGroundColor.value = R.color.colorPrimary
        }
        val rand = Random()
        number1.value = rand.nextInt(9) + 1
        number2.value = rand.nextInt(9) + 1
        if (rand.nextBoolean()) {
            number3.value = number1.value as Int + number2.value as Int
            positiveAnswer = true
        } else {
            var value = 0
            while (value == 0) {
                value = rand.nextInt(4) - 1
            }
            Log.d("Random value", String.format("%d", value))
            number3.value = number1.value as Int + number2.value as Int + value
            positiveAnswer = false
        }
    }

    fun onChooseAnswer(chosenButton: Boolean) {
        if (chosenButton == positiveAnswer) {
            currentScore.value = currentScore.value?.plus(1)
            refreshQuestion()
        } else {
            val cur = currentScore.value
            cur?.let {
                val highScore = AppDataUtil.getEasyHighScore()
                highScore?.let {
                    if (cur >= highScore) {
                        AppDataUtil.setEasyHighScore(cur)
                    }
                    showDialog.value = "Wrong"
                }
            }
        }
    }

    fun newGame() {
        currentScore.value = 0
        backGroundColor.value = FunMathUtil.getRandomColor()
        refreshQuestion()
    }
}