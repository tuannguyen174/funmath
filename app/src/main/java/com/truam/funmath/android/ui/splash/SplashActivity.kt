package com.truam.funmath.android.ui.splash

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.ads.AdRequest
import com.truam.funmath.android.R
import com.truam.funmath.android.databinding.ActivitySplashBinding
import com.truam.funmath.android.ui.easy.EasyModeActivity
import com.truam.funmath.android.ui.normal.FactoryModeActivity
import com.truam.funmath.android.util.AppDataUtil
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


/**
 * Copyright © 2017 Truam Studio.
 * All Rights Reserved
 *
 * Created on 19-Dec-17.
 * by Nguyen Phuong Tuan
 */

class SplashActivity : AppCompatActivity() {

    private var binding: ActivitySplashBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/TextFont.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )

        binding?.buttonEasy?.setOnClickListener {
            startActivity(Intent(this, EasyModeActivity::class.java))
        }

        binding?.buttonNormal?.setOnClickListener {
            val intent = Intent(this, FactoryModeActivity::class.java)
            intent.putExtra(FactoryModeActivity.EXTRA_MODE, FactoryModeActivity.EXTRA_MODE_NORMAL)
            startActivity(intent)
        }

        binding?.buttonHard?.setOnClickListener {
            val intent = Intent(this, FactoryModeActivity::class.java)
            intent.putExtra(FactoryModeActivity.EXTRA_MODE, FactoryModeActivity.EXTRA_MODE_HARD)
            startActivity(intent)
        }

        binding?.buttonInfo?.setOnClickListener {
            showInfoDialog()
        }

        val adRequest = AdRequest.Builder().build()
        binding?.adView?.loadAd(adRequest)
    }

    override fun onResume() {
        super.onResume()
        val easyBestScore = AppDataUtil.getEasyHighScore()
        easyBestScore?.let {
            binding?.textViewEasyBestScore?.text = String.format("%d", easyBestScore)
        }

        val normalBestScore = AppDataUtil.getNormalHighScore()
        normalBestScore?.let {
            binding?.textViewNormalBestScore?.text = String.format("%d", normalBestScore)
        }

        val hardBestScore = AppDataUtil.getHardHighScore()
        hardBestScore?.let {
            binding?.textViewHardBestScore?.text = String.format("%d", hardBestScore)
        }

    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun showInfoDialog() {
        AlertDialog.Builder(this, R.style.InfoDialogStyle)
                .setTitle(getString(R.string.info_dialog_title))
                .setMessage(getString(R.string.info_dialog_message))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.info_dialog_ok), { _, _ ->})
                .setNegativeButton(getString(R.string.info_dialog_more), { _, _ ->
                    gotoPlayStore()
                })
                .show()
    }

    private fun gotoPlayStore() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://developer?id=Truam+Studio")))
        } catch (ex: android.content.ActivityNotFoundException) {
            ex.printStackTrace()
            startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/developer?id=Truam+Studio")))
        }

    }
}
