package com.truam.funmath.android.ui.normal

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.os.CountDownTimer
import android.util.Log
import com.truam.funmath.android.R
import com.truam.funmath.android.util.AppDataUtil
import com.truam.funmath.android.util.FunMathUtil
import java.util.*


/**
 * Copyright © 2017 Truam Studio.
 * All Rights Reserved
 *
 * Created on 19-Dec-17.
 * by Nguyen Phuong Tuan
 */

class FactoryModeViewModel : ViewModel() {
    var number1 = MutableLiveData<Int>()
    var number2 = MutableLiveData<Int>()
    var number3 = MutableLiveData<Int>()
    var currentScore = MutableLiveData<Int>()
    var showDialog = MutableLiveData<String>()
    var progress = MutableLiveData<Int>()
    var backGroundColor = MutableLiveData<Int>()
    private var countDownTimer: CountDownTimer? = null
    private var positiveAnswer = true
    private var maxTime = 1900L
    private var subTime = 20
    private var currentMode = FactoryModeActivity.EXTRA_MODE_NORMAL

    fun setModeInfo(mode: String) {
        if (mode == FactoryModeActivity.EXTRA_MODE_HARD) {
            maxTime = 1200L
            subTime = 40
            currentMode = FactoryModeActivity.EXTRA_MODE_HARD
        }
        else {
            maxTime = 1900L
            subTime = 20
            currentMode = FactoryModeActivity.EXTRA_MODE_NORMAL
        }
    }

    fun refreshQuestion() {
        if (currentScore.value == null) {
            currentScore.value = 0
        }
        if (backGroundColor.value == null) {
            backGroundColor.value = R.color.colorPrimary
        }
        if (progress.value == null) {
            progress.value = 0
        }
        val rand = Random()
        number1.value = rand.nextInt(9) + 1
        number2.value = rand.nextInt(9) + 1
        if (rand.nextBoolean()) {
            number3.value = number1.value as Int + number2.value as Int
            positiveAnswer = true
        } else {
            var value = 0
            while (value == 0) {
                value = rand.nextInt(4) - 1
            }
            number3.value = number1.value as Int + number2.value as Int + value
            positiveAnswer = false
        }
    }

    fun onChooseAnswer(chosenButton: Boolean) {
        countDownTimer?.cancel()
        if (chosenButton == positiveAnswer) {
            currentScore.value = currentScore.value?.plus(1)
            refreshQuestion()
            runCountDownTimer()
        } else {
            checkHighScore()
            showDialog.value = "Wrong"
        }
    }

    fun newGame() {
        progress.value = 0
        currentScore.value = 0
        backGroundColor.value = FunMathUtil.getRandomColor()
        refreshQuestion()
    }

    private fun runCountDownTimer() {
        val curScoreTime = currentScore.value?.times(subTime)?.toLong()
        var timer = maxTime
        curScoreTime?.let {
            timer -= curScoreTime
        }
        Log.d("runCountDownTimer", String.format("%d", timer))
        countDownTimer = object : CountDownTimer(timer, 18) {
            override fun onTick(p0: Long) {
                progress.value = ((timer - p0) / (timer / 100)).toInt()
            }

            override fun onFinish() {
                progress.value = 100
                checkHighScore()
                showDialog.value = "Time out"
            }
        }
        countDownTimer?.start()
    }

    private fun checkHighScore() {
        val cur = currentScore.value
        cur?.let {
            if (currentMode == FactoryModeActivity.EXTRA_MODE_HARD) {
                val highScore = AppDataUtil.getHardHighScore()
                highScore?.let {
                    if (cur >= highScore) {
                        AppDataUtil.setHardHighScore(cur)
                    }
                }
            }
            else {
                val highScore = AppDataUtil.getNormalHighScore()
                highScore?.let {
                    if (cur >= highScore) {
                        AppDataUtil.setNormalHighScore(cur)
                    }
                }
            }
        }
    }
}