package com.truam.funmath.android.util

import android.content.Context
import android.content.SharedPreferences

/**
 * Copyright © 2017 Truam Studio.
 * All Rights Reserved
 *
 * Created on 19-Dec-17.
 * by Nguyen Phuong Tuan
 */

class AppDataUtil {
    companion object {
        private val SHARED_PREFERENCES_KEY = "APP_DATA"
        private val KEY_EASY_HIGH_SCORE = "KEY_EASY_HIGH_SCORE"
        private val KEY_NORMAL_HIGH_SCORE = "KEY_NORMAL_HIGH_SCORE"
        private val KEY_HARD_HIGH_SCORE = "KEY_HARD_HIGH_SCORE"

        var sharedPreferences: SharedPreferences? = null

        fun setContext(context: Context) {
            sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE)
        }

        fun getEasyHighScore(): Int? {
            return sharedPreferences?.getInt(KEY_EASY_HIGH_SCORE, 0)
        }

        fun setEasyHighScore(value: Int) {
            sharedPreferences?.edit()?.putInt(KEY_EASY_HIGH_SCORE, value)?.apply()
        }

        fun getNormalHighScore(): Int? {
            return sharedPreferences?.getInt(KEY_NORMAL_HIGH_SCORE, 0)
        }

        fun setNormalHighScore(value: Int) {
            sharedPreferences?.edit()?.putInt(KEY_NORMAL_HIGH_SCORE, value)?.apply()
        }

        fun getHardHighScore(): Int? {
            return sharedPreferences?.getInt(KEY_HARD_HIGH_SCORE, 0)
        }

        fun setHardHighScore(value: Int) {
            sharedPreferences?.edit()?.putInt(KEY_HARD_HIGH_SCORE, value)?.apply()
        }
    }
}