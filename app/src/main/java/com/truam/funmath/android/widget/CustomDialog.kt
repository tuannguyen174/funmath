package com.truam.funmath.android.widget

import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.truam.funmath.android.R
import com.truam.funmath.android.databinding.DialogCustomBinding

/**
 * Copyright © 2017 GNT Inc.
 * All Rights Reserved
 *
 * Created on 12/20/17.
 * by Nguyen Phuong Tuan
 * tuan.np@vn.gnt-global.com
 */


class CustomDialog : DialogFragment() {

    private var buttonPlayAgainOnClick: View.OnClickListener? = null

    private var binding: DialogCustomBinding? = null

    companion object {
        val EXTRA_TITLE = "EXTRA_TITLE"
        val EXTRA_SCORE = "EXTRA_SCORE"
        val EXTRA_HIGH_SCORE = "EXTRA_HIGH_SCORE"

        fun newInstance(title: String, score: String, highScore: String): CustomDialog {
            val infoDialogFragment = CustomDialog()
            val bundle = Bundle()
            bundle.putString(EXTRA_TITLE, title)
            bundle.putString(EXTRA_SCORE, score)
            bundle.putString(EXTRA_HIGH_SCORE, highScore)
            infoDialogFragment.arguments = bundle

            return infoDialogFragment
        }
    }

    fun setOnClickListener(buttonOnClickListener: View.OnClickListener) {
        buttonPlayAgainOnClick = buttonOnClickListener
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_custom, container, false)
        return binding?.root
    }

    override fun onStart() {
        super.onStart()

        val window = dialog.window
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val args = arguments ?: return
        if (args.containsKey(EXTRA_SCORE)) {
            binding?.textViewNewScore?.text = arguments.getString(EXTRA_SCORE)
        }
        if (args.containsKey(EXTRA_HIGH_SCORE)) {
            binding?.textViewBestScore?.text = arguments.getString(EXTRA_HIGH_SCORE)
        }
        if (args.containsKey(EXTRA_TITLE)) {
            binding?.textViewTitle?.text = arguments.getString(EXTRA_TITLE)
        }
        if (args.containsKey(EXTRA_SCORE) && args.containsKey(EXTRA_HIGH_SCORE) && Integer.parseInt(args.getString(EXTRA_SCORE)) >= Integer.parseInt(args.getString(EXTRA_HIGH_SCORE))) {
            binding?.textViewNewBest?.visibility = View.VISIBLE
            val animation = AnimationUtils.loadAnimation(context, R.anim.new_high_score_anim)
            binding?.textViewNewBest?.animation = animation
            animation.start()
        } else {
            binding?.textViewNewBest?.visibility = View.GONE
        }

        binding?.buttonPlayAgain?.setOnClickListener(buttonPlayAgainOnClick)

        isCancelable = false
    }
}