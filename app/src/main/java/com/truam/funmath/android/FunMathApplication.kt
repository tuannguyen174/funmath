package com.truam.funmath.android

import android.app.Application
import com.google.android.gms.ads.MobileAds
import com.truam.funmath.android.util.AppDataUtil


/**
 * Copyright © 2017 GNT Inc.
 * All Rights Reserved
 *
 * Created on 12/20/17.
 * by Nguyen Phuong Tuan
 * tuan.np@vn.gnt-global.com
 */

/**Launcher
 * Link: https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html
 * Text: 1+1=?
 * Font: Days One
 * Padding 20%
 * Color: white
 * Background: #26a69a
 * Scaling: center
 * Shape: wide rect
 * Effect: none
 **/

/**Font Info:
 * TextFont = Audiowide
 * NumberFont = DayOne
 */


class FunMathApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        AppDataUtil.setContext(this.applicationContext)

        MobileAds.initialize(this, "ca-app-pub-4412751318966463~9341438756")
    }
}
