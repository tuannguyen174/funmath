package com.truam.funmath.android.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView


/**
 * Copyright © 2017 Truam Studio.
 * All Rights Reserved
 *
 * Created on 26-Dec-17.
 * by Nguyen Phuong Tuan
 */


class SquareImageView : ImageView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, int: Int) : super(context, attrs, int)

    override fun onMeasure(width: Int, height: Int) {
        super.onMeasure(width, height)
        val measuredWidth = measuredWidth
        setMeasuredDimension(measuredWidth, measuredWidth)
    }
}