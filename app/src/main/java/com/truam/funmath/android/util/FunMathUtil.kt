package com.truam.funmath.android.util

import com.truam.funmath.android.R
import java.util.*


/**
 * Copyright © 2017 GNT Inc.
 * All Rights Reserved
 *
 * Created on 12/25/17.
 * by Nguyen Phuong Tuan
 * tuan.np@vn.gnt-global.com
 */


class FunMathUtil {
    companion object {
        fun getRandomColor(): Int {
            val rand = Random()
            when (rand.nextInt(8)) {
                0 -> return R.color.colorBackground1
                1 -> return R.color.colorBackground2
                2 -> return R.color.colorBackground3
                3 -> return R.color.colorBackground4
                4 -> return R.color.colorBackground5
                5 -> return R.color.colorBackground6
                6 -> return R.color.colorBackground7
                7 -> return R.color.colorBackground8
            }
            return R.color.colorBackground9
        }
    }
}